import sys
import zmq
import re
import io
import time
import numpy as np
import matplotlib
matplotlib.use('TKAgg')
from matplotlib import pyplot as plt
from matplotlib import animation

def green(port = 5556):

    # Socket to talk to server
    context = zmq.Context()
    socket = context.socket(zmq.SUB)

    # socket.connect ("tcp://172.29.231.48:%s" % port)
    socket.connect ("tcp://localhost:%s" % port)

    topicfilter = "42"
    socket.setsockopt(zmq.SUBSCRIBE, topicfilter)

    filenew = open("output.txt","w")

    numOfWords = 0
    totalSentence = 0
    totalWord = 0
    wordInSentence = 0

    charInWord = 0
    totalChar = 0

    pronounList1 = ['yous guys', 'c++', 'shrubbery', 'star trek','we', 'us', 'you', 'they', 'them']
    pronounDict1 = {}
    for p in pronounList1:
        pronounDict1[p] = 0

    lengthOfLongWord = 7
    lengthOfLongSentence = 15

    numOfLongWord = 0
    numOfLongSentence = 0

    collectPronoun = 0
    wordsGreenLike = 0
    pointOfGreen = 0

    sentence_end_finder = re.compile(r"[\.\?!]")
    message_data = ''

    # Process updates
    total_value = 0
    for update_nbr in range (300):
        string = socket.recv()
        words = string.split()[1:]

        for word in words:
            charInWord = len(word)
            totalChar += charInWord
            wordInSentence += 1
            if(charInWord > lengthOfLongWord):
                numOfLongWord += 1

            if (word in pronounList1):
                pronounDict1[word] += 1
            wordsGreenLike = 0
            for key1 in pronounDict1:
                wordsGreenLike += pronounDict1[key1]

            if (sentence_end_finder.findall(word)):
                if (wordInSentence >= lengthOfLongSentence):
                    numOfLongSentence +=1

        pointOfGreen = numOfLongSentence * 10 + wordsGreenLike * 10 + numOfLongWord * 1
        print "pointOfGreen", pointOfGreen

if __name__ == "__main__":
    green(5556);





