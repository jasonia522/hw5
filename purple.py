import sys
import zmq
import re
import io
import time
import numpy as np
import matplotlib
matplotlib.use('TKAgg')
from matplotlib import pyplot as plt
from matplotlib import animation

def purple(port = 5556):

    # Socket to talk to server
    context = zmq.Context()
    socket = context.socket(zmq.SUB)

    # socket.connect ("tcp://172.29.231.48:%s" % port)
    socket.connect ("tcp://localhost:%s" % port)

    topicfilter = "42"
    socket.setsockopt(zmq.SUBSCRIBE, topicfilter)

    filenew = open("output.txt","w")

    numOfWords = 0
    totalSentence = 0
    totalWord = 0
    wordInSentence = 0
    wordPerSentence = 0

    charInWord = 0
    totalChar = 0

    pronounList3 = ['y\'all', 'python', 'hedge', 'star wars', 'i', 'me', 'you', 'he', 'him', 'she', 'her', 'it']
    pronounDict3 = {}
    for q in pronounList3:
        pronounDict3[q] = 0

    lengthOfShortWord = 3
    lengthOfShortSentence = 10

    numOfShortWord = 0
    numOfShortSentence = 0

    indiviPronoun = 0
    wordsPurpleLike = 0
    pointOfPurple = 0

    sentence_end_finder = re.compile(r"[\.\?!]")
    message_data = ''

    # Process updates
    total_value = 0
    for update_nbr in range (300):
        string = socket.recv()
        words = string.split()[1:]
        print " ".join(words)

        numberOfWords = len(words)
        totalWord += numberOfWords

        for word in words:
            charInWord = len(word)
            totalChar += charInWord
            wordInSentence += 1
            if(charInWord < lengthOfShortWord):
                numOfShortWord += 1

            if (word in pronounList3):
                pronounDict3[word] += 1
            wordsPurpleLike = 0
            for key3 in pronounDict3:
                wordsPurpleLike += pronounDict3[key3]

            if (sentence_end_finder.findall(word)):
                if (wordInSentence <= lengthOfShortSentence):
                    numOfShortSentence += 1
                totalSentence += 1
                wordInSentence = 0

        pointOfGreen = numOfLongSentence * 10 + wordsGreenLike * 10 + numOfLongWord * 1
        pointOfPurple = numOfShortSentence * 10 + wordsPurpleLike * 10 + numOfShortWord * 1

        print "pointOfPurple", pointOfPurple

if __name__ == "__main__":
    purple(5556);





