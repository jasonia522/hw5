import sys
import zmq
import re
import io
import time
import numpy as np
import matplotlib
matplotlib.use('TKAgg')
from matplotlib import pyplot as plt
from matplotlib import animation

def plot(port = 5556):
    # Socket to talk to server
    context = zmq.Context()
    socket = context.socket(zmq.SUB)

    print "Listening in to the broadcast..."
    # socket.connect ("tcp://172.29.231.48:%s" % port)
    socket.connect ("tcp://localhost:%s" % port)

    topicfilter = "42"
    socket.setsockopt(zmq.SUBSCRIBE, topicfilter)

    filenew = open("output.txt","w")

    numOfWords = 0
    totalSentence = 0
    totalWord = 0
    wordInSentence = 0
    wordPerSentence = 0
    maxWordPerSentence = 0
    minWordPerSentence = 1000

    charInWord = 0
    totalChar = 0
    maxCharLength = 0
    minCharLength = 1000
    avgCharLength = 0

    pronounList1 = ['yous guys', 'c++', 'shrubbery', 'star trek','we', 'us', 'you', 'they', 'them']
    pronounList3 = ['y\'all', 'python', 'hedge', 'star wars', 'i', 'me', 'you', 'he', 'him', 'she', 'her', 'it']
    pronounDict1 = {}
    for p in pronounList1:
        pronounDict1[p] = 0
    pronounDict3 = {}
    for q in pronounList3:
        pronounDict3[q] = 0

    lengthOfLongWord = 7
    lengthOfLongSentence = 15
    lengthOfShortWord = 3
    lengthOfShortSentence = 10

    numOfLongWord = 0
    numOfLongSentence = 0
    numOfShortWord = 0
    numOfShortSentence = 0

    collectPronoun = 0
    indiviPronoun = 0
    wordsGreenLike = 0
    wordsPurpleLike = 0
    pointOfGreen = 0
    pointOfPurple = 0

    dataround = []
    greenPoint = []
    purplePoint = []

    sentence_end_finder = re.compile(r"[\.\?!]")
    message_data = ''

    plt.axis([0, 30, 0, 300])
    plt.ion()
    plt.show()
    x = 0

    # Process updates
    total_value = 0
    for update_nbr in range (300):
        string = socket.recv()
        words = string.split()[1:]
        print " ".join(words)

        numberOfWords = len(words)
        totalWord += numberOfWords

        for word in words:
            charInWord = len(word)
            totalChar += charInWord
            wordInSentence += 1
            if(charInWord > maxCharLength):
                maxCharLength = charInWord
            if(charInWord < minCharLength):
                minCharLength = charInWord
            if(charInWord > lengthOfLongWord):
                numOfLongWord += 1
            if(charInWord < lengthOfShortWord):
                numOfShortWord += 1

            if (word in pronounList1):
                pronounDict1[word] += 1
            wordsGreenLike = 0
            for key1 in pronounDict1:
                wordsGreenLike += pronounDict1[key1]

            if (word in pronounList3):
                pronounDict3[word] += 1
            wordsPurpleLike = 0
            for key3 in pronounDict3:
                wordsPurpleLike += pronounDict3[key3]

            if (sentence_end_finder.findall(word)):
                if (wordInSentence > maxWordPerSentence):
                    maxWordPerSentence = wordInSentence
                if (wordInSentence < minWordPerSentence):
                    minWordPerSentence = wordInSentence
                if (wordInSentence >= lengthOfLongSentence):
                    numOfLongSentence +=1
                if (wordInSentence <= lengthOfShortSentence):
                    numOfShortSentence += 1
                totalSentence += 1
                wordInSentence = 0

        wordPerSentence = totalWord * 1.0 / totalSentence
        charPerWord = totalChar * 1.0 / totalWord

        pointOfGreen = numOfLongSentence * 10 + wordsGreenLike * 10 + numOfLongWord * 1
        pointOfPurple = numOfShortSentence * 10 + wordsPurpleLike * 10 + numOfShortWord * 1

        print '\n', \
          "numOfLongSentence", numOfLongSentence, '\n', \
          "numOfShortSentence", numOfShortSentence, '\n', \
          "numOfLongWord", numOfLongWord, '\n', \
          "numOfShortWord", numOfShortWord, '\n', \
          "wordsGreenLike", wordsGreenLike, '\n', \
          "wordsPurpleLike", wordsPurpleLike, '\n', \
          "pointOfGreen", pointOfGreen, '\n', \
          "pointOfPurple", pointOfPurple
          # "totalWord:", totalWord, '\n', \
          # "totalSentence:", totalSentence, '\n', \
          # "maxWordPerSentence:", maxWordPerSentence, '\n', \
          # "minWordPerSentence:", minWordPerSentence, '\n', \
          # "wordPerSentence", wordPerSentence, '\n', \
          # "totalChar", totalChar, '\n', \
          # "maxCharLength", maxCharLength, '\n', \
          # "minCharLength", minCharLength, '\n', \
          # "avgCharLength", avgCharLength, '\n', \

        x += 1
        dataround.append(x)
        greenPoint.append(pointOfGreen)
        purplePoint.append(pointOfPurple)
        plt.scatter(x, pointOfGreen)
        plt.plot(dataround, greenPoint, '-go', dataround, purplePoint, '-mo')
        plt.draw()

        if (x > 20):
            plt.axis([0, x + 10, 0, max(pointOfGreen, pointOfPurple) + 100])

if __name__ == "__main__":
    plot(5556);





