from multiprocessing import Process
import multiprocessing
from broadcaster import *
from green import *
from purple import *
from plot import *

if __name__ == "__main__":
	speech_port = 5556
	Process(target = broadcast, args = ("speech1.txt", speech_port,)).start()
	# Process(target = broadcast, args = ("speech2.txt", speech_port,)).start()
	Process(target = green, args = (speech_port,)).start()
	Process(target = purple, args = (speech_port,)).start()
	plot()

